# Lightweight command line tools for automated testing

_Yet another one!?!_ **Yes!** One day of searching for such tool ended up in two days of making one. This tool is an answer to a simple requirement:

> Have the ability to run a set of external programs, and check certain results, like output files, standard output, etc., and all that by using a something _small_, _not_ dependent on a heap of other assemblies.

The `Release` version of `CmdRunTests.exe` is **22KB**. It relies only on _.NET Framework 4_ provided assemblies. The last part of the requirement is fulfilled, the first is ...

## ... configuring the external programs tests

It is done by a simple JSON file, which looks like this:

```json
[
  {
    "name": "Preparation task",
    "prepare": "if not exist \"C:\\TestsOutput\\\" mkdir C:\\TestsOutput",
    "cleanup": "del /Q C:\\TestsOutput\\*",
    "wait":  true
  }, 
  {
    "name": "Simple encrypt & decrypt",
    "exe": "dtexec.exe",
    "arguments": "/proj S:\\TestPackages\\Packages\\{{SQL}}.Deployment\\StreamingData.ispac /package EncryptDecrypt.dtsx",
    "cleanup": "del /Q C:\\TestsOutput\\Zipped\\* && del /Q C:\\TestsOutput\\thyroid_out.zip",
    "expectations": [
      {
        "file": "C:\\TestsOutput\\thyroid_out.zip",
        "size": "55KB"
      },
      {
        "exit_code": 0,
        "output": "[OK]"
      },
      {
        "file": "C:\\TestsOutput\\Zipped\\thyroid_train.csv",
        "SHA1": "LTIsRMT/I2+U3ULJUBNZXmKsKhs=",
        "size": "275519"
      }
    ],
    "stdout_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "stderr_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "report": "default"
  },
  {
    "name": "Streaming encrypt & decrypt",
    "exe": "dtexec.exe",
    "arguments": "/proj S:\\TestPackages\\Packages\\{{SQL}}.Deployment\\StreamingData.ispac /package Streaming.dtsx",
    "cleanup": "del /Q C:\\TestsOutput\\Streamed\\*",
    "expectations": [
      {
        "exit_code": 0,
        "output": "[OK]"
      },
      {
        "file": "C:\\TestsOutput\\Streamed\\thyroid_train.csv",
        "SHA1": "LTIsRMT/I2+U3ULJUBNZXmKsKhs=",
        "size": "275519"
      }
    ],
    "stdout_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "stderr_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "report": "default"
  },
  {
    "name": "Tableau Destination",
    "arguments": "/proj S:\\TestPackages\\Packages\\{{SQL}}.Deployment\\TableauDestination.ispac /package Tableau.dtsx",
    "exe": "dtexec.exe",
    "cleanup": "del /Q C:\\TestsOutput\\tableau_out.tde",
    "expectations": [
      {
        "exit_code": 0,
        "output": "[OK]"
      },
      {
        "file": "C:\\TestsOutput\\tableau_out.tde",
        "SHA1": "4mFydzC/w6sVozY5/imkLAiJVpg=",
        "size": "104KB"
      }
    ],
    "stdout_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "stderr_parsers": [ "dtexec_ok", "dtexec_failed", "cleaner" ],
    "timeout": 15,
    "report": "default"
  }
]
```

Most of the parts a pretty clear. There are two specific moments. One is the use of _hashing_ instead of direct compare. The rationale is that (a) compare could be more time consuming, and (b) it'll require having additional set of _correct_ output files to compare with. With this approach the only source file for testing is this JSON.

The second specific moment are ...

## ... the parsers

Sometimes programs' outputs are too big to compare, and in the same time they come in a unified way. Thus, the idea of having standard set of parsers and just specify which and in what order to be applied to the `stderr` and `stdout` streams. A parsers/formatters configuration file looks like this:

```json
{
  "parsers": [
    {
      "name": "dtexec_ok",
      "regex": "/.*DTSER_SUCCESS \\(0\\).*/[OK]/",
      "multilined": false
    },
    {
      "name": "dtexec_failed",
      "regex": "/.*DTSER_FAILURE \\(([0-9]+)\\).*/[Err: $1]/",
      "multilined": false
    }
  ],
  "formatters": [
    {
      "name": "gitlab",
      "OK": "{{task.name}}: [OK]",
      "Failure": "{{task.name}}: [Err]\n{{explanation}}"
    }
  ]
}
```

Happy using,

Ivan (Jonan) Georgiev @ Volley Labs Ltd.


### TODO:

* Have the ability to provide many configuration and test definition files.

* Wrap the functionality in a separate Class Library to be used by the tool(s).

* Develop the `CmdWaitFor` tool, with possible uses like:

  * `CmdWaitFor hkey-to-appear HKEY_LOCAL_MACHINE\ … \`

  * `CmdWaitFor hkey-to-equal HKEY_LOCAL_MACHINE\ … \"Test value"`

  * `CmdWaitFor hkey-to-pass HKEY_LOCAL_MACHINE\ … \ 6`

  * `CmdWaitFor file-to-appear <path>`

  * `CmdWaitFor file-to-change <path>`

  * `CmdWaitFor file-to-grow <path>`

  * `CmdWaitFor file-to-equal <path> <string>`
  
  * `CmdWaitFor file-to-hash <path> <hash>`
  
  * ...
