﻿/**
 * Data Serialization contracts for JSON configuration and 
 * test definition files.
 * 
 * Copyright (C) 2017, Volley Labs Ltd.
 * Author: Ivan (Jonan) Georgiev
 */
using System.Runtime.Serialization;

namespace CmdRunTests
{
#pragma warning disable CS0649

    #region Task description contracts
    [DataContract]
    internal class TaskExpectation
    {
        [DataMember]
        public string file;
        [DataMember]
        public string SHA1;
        [DataMember]
        public object exit_code;
        [DataMember]
        public string output;
        [DataMember]
        public string error;
        [DataMember]
        public string size;
    }

    [DataContract]
    internal class TaskDescription
    {
        [DataMember(IsRequired = true)]
        public string name;
        [DataMember]
        public string exe;
        [DataMember]
        public string arguments;
        [DataMember]
        public string working_dir;
        [DataMember]
        public bool synchronous;
        [DataMember]
        public double timeout;
        [DataMember]
        public string prepare;
        [DataMember]
        public string cleanup;
        [DataMember]
        public bool wait;

        [DataMember]
        public string stdin_string;
        [DataMember]
        public string stdin_file;

        [DataMember]
        public string stdout_file;
        [DataMember]
        public string[] stdout_parsers;
        [DataMember]
        public string stderr_file;
        [DataMember]
        public string[] stderr_parsers;

        [DataMember]
        public TaskExpectation[] expectations;
        [DataMember]
        public string report;
    };

    #endregion

    #region Configuration contracts

    [DataContract]
    internal class ParserDescription
    {
        [DataMember(IsRequired = true)]
        public string name;

        [DataMember(IsRequired = true)]
        public string regex;

        [DataMember]
        public bool multilined;

    }

    [DataContract]
    internal class FormatterDescription
    {
        [DataMember(IsRequired = true)]
        public string name;

        [DataMember]
        public string OK;

        [DataMember]
        public string Failure;
    }

    [DataContract]
    internal class ConfigDescription
    {
        [DataMember]
        public ParserDescription[] parsers;

        [DataMember]
        public FormatterDescription[] formatters;
    }

    #endregion

#pragma warning restore CS0649
}
