﻿/**
 * The CmdRunTests execution code.
 * 
 * Copyright (C) 2017, Volley Labs Ltd.
 * Author: Ivan (Jonan) Georgiev
 */
using System;
using System.IO;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading;
using CmdRunTests;

namespace CmdTestTools
{
    internal class ParserDefinition
    {
        public Regex pattern
        { get; set; }

        public string subst
        { get; set; }

        public bool multilined
        { get; set; }

        public ParserDefinition(Regex regex, string substitution, bool multi)
        {
            this.pattern = regex;
            this.subst = substitution;
            this.multilined = multi;
        }
    }

    class CmdTesterProgram
    {
        static private void ShowHelp()
        {
            Console.WriteLine("Lightweight command line testing tool.\nCopyright (C) 2017, VolleyLabs Ltd.\n");
            Console.WriteLine("Usage: CmdTester [<options>] <tests-description-json>");
            Console.WriteLine("       CmdTester /h");
            Console.WriteLine("\nOptions:");
            Console.WriteLine("  /c[onfiguration] <config-json>    Specify the parsers and formatters configuration JSON file.");
            Console.WriteLine("  /t[imeout] <seconds>              The default timeout for each of the tasks, in seconds. Default is 10.");
            Console.WriteLine("  /l[log] <log file>                Provide a path for log file, for tests stdout/stderr.");
            Console.WriteLine("  /k[ey]                            Provide key:value pair for substituion in tests description file.");
            Console.WriteLine("                                    Can have multiple such entries.");
            Console.WriteLine("  /w[ait]                           Make the tasks wait for each other, i.e. - run synchronously.");
            Console.WriteLine("  /p[reserve]                       Preserve the outputs, i.e. don't run the `cleanup` step.");
            Console.WriteLine("  /? | /h[elp]                      Output this help information.");
            Console.WriteLine("  <tests-description-json>          The mandatory tests description JSON file.");
            Console.WriteLine();
        }

        private static int ShellTask(string command)
        {
            if (command != null)
            {
                Runner runner = new Runner("cmd", "/c " + command, RunnerMode.Synchronous);
                runner.Start();
                return runner.ExitCode;
            }
            else
                return 0;
        }

        private static void ParseKeyValue(string pair, Dictionary<string, string> dict)
        {
            string[] couple = pair.Trim().Split(new char[] { ':' });
            if (couple.Length != 2)
                throw new FormatException("Invalid key-value pair: `" + pair + "` !");
            dict.Add("{{" + couple[0] + "}}", couple[1]);
        }

        private static Dictionary<string, string> BuildTaskDictionary(TaskDescription task, string explanation)
        {
            return new Dictionary<string, string>
            {
                { "{{task.name}}", task.name },
                { "{{task.exe}}", task.exe },
                { "{{task.arguments}}", task.arguments },
                { "{{task.report}}", task.report },
                { "{{explanation}}", explanation }
            };
        }

        private const double DEF_TIMEOUT = 60.0;

        static int Main(string[] args)
        {
            try
            {
                string testset = null;
                string configuration = null;
                string logpath = null;
                double timeout = DEF_TIMEOUT;
                bool preserve = false;
                bool wait = false;
                Dictionary<string, string> keyValues = new Dictionary<string, string>();

                for (int i = 0; i < args.Length; ++i)
                {
                    var arg = args[i];
                    if (arg.StartsWith("/c")) // config argument
                        configuration = args[++i];
                    else if (arg.StartsWith("/l")) // log file argument
                        logpath = args[++i];
                    else if (arg.StartsWith("/w")) // synchronous flag argument
                        wait = true;
                    else if (arg.StartsWith("/p")) // preserve the outputs
                        preserve = true;
                    else if (arg.StartsWith("/h") || arg.Equals("/?")) // asking for help
                        throw new Exception("");
                    else if (arg.StartsWith("/t")) // the default timeout
                        timeout = Double.Parse(args[++i]);
                    else if (arg.StartsWith("/k")) // key-value pair specification
                        ParseKeyValue(args[++i], keyValues);
                    else
                        testset = arg;
                }

                if (testset == null)
                    throw new InvalidProgramException("You cannot skip specifying <tests-description-json> argument!");

                TextWriter logfile = null;
                if (logpath != null)
                {
                    var fstr = File.OpenWrite(logpath);
                    logfile = new StreamWriter(fstr);
                }

                CmdTesterProgram prg = new CmdTesterProgram(
                    keyValues,
                    logfile,
                    testset, 
                    configuration, 
                    timeout,
                    wait, 
                    preserve);

                if (logfile != null)
                    logfile.Close();

                Console.WriteLine("Finished!");
                return prg.Result;
            }
            catch (Exception e)
            {
                ShowHelp();
                if (e.Message.Length > 0)
                {
                    Console.Error.WriteLine("Err: {0}", e.Message);
#if DEBUG
                    for (; e != null; e = e.InnerException)
                    {
                        Console.Error.WriteLine("----------");
                        Console.Error.WriteLine(e.Message);
                        Console.Error.WriteLine(e.StackTrace);
                    }
#endif
                }
                else
                    Console.Error.WriteLine("Enjoy!");
                return -1;
            }
        }

        #region Instance execution

        private Dictionary<string, ParserDefinition> _parsers = new Dictionary<string, ParserDefinition>();
        private Dictionary<string, string[]> _formatters = new Dictionary<string, string[]>();
        private Dictionary<string, string> _keys;
        private TaskDescription[] _tasks;
        private TextWriter _logger;


        private const string FORMATDEF_NAME = @"default";
        private const string FORMATDEF_OK = @"{{task.name}}: [Success]";
        private const string FORMATDEF_FAILURE = @"{{task.name}}: [Failure]\n\r{{explanation}}";
        private const string PARSECLEAN_NAME = @"cleaner";
        private const string PARSECLEAN_REGEX = @"/.*//";
        private const string DEF_PREPARE_EXPLAIN = @"Preparation command error!";

        private const int RESULT_OK = 0;
        private const int RESULT_ERR = 1;

        private CmdTesterProgram(
            Dictionary<string, string> keyValues,
            TextWriter logfile,
            string testet, 
            string configuration, 
            double timeout,
            bool wait,
            bool preserve)
        {
            _keys = keyValues;
            _logger = logfile;
            // Add the default formatters and parsers now, so it can be overriden, if desired afterwards.
            _formatters.Add(FORMATDEF_NAME, new string[] { FORMATDEF_OK, FORMATDEF_FAILURE });
            _parsers.Add(PARSECLEAN_NAME, ParseRegexDefinition(PARSECLEAN_REGEX, false));

            if (configuration != null)
                LoadConfiguration(configuration);

            // Go for task deserialization and process it.
            var serializer = new DataContractJsonSerializer(typeof(TaskDescription[]));
            using (FileStream taskFile = File.Open(testet, FileMode.Open, FileAccess.Read))
                _tasks = (TaskDescription[])serializer.ReadObject(taskFile);

            // Some parallelism housekeeping stuff - the reproting synchronizer and the signal-finish event
            object reportGate = new object();

            ManualResetEvent finishedEvent = new ManualResetEvent(false);
            int finishedCount = 0;

            // Define our runner finish delegate.
            Runner.RunnerDone onFinish = delegate (Runner runner, bool killed)
            {
                TaskDescription task = (TaskDescription)runner.UserInfo;
                string explanation;
                int resultIndex;

                if (!killed)
                {
                    // Parse the task output and error streams and check whether expectations are met.
                    string taskOutput = ProcessStream(runner.Output, task.stdout_parsers, GetEnrichedValue(task.stdout_file));
                    string taskError = ProcessStream(runner.Error, task.stderr_parsers, GetEnrichedValue(task.stderr_file));

                    resultIndex = CheckExpectations(task, runner.ExitCode, taskOutput, taskError, out explanation);
                }
                else
                {
                    explanation = "Timed out!";
                    resultIndex = RESULT_ERR;
                }
                if (_logger != null)
                    _logger.WriteLine("<<< [" + task.name + "]");

                // Do the cleaning, if such is provided.
                if (!preserve)
                    ShellTask(GetEnrichedValue(task.cleanup));

                lock (reportGate)
                {
                    Console.Out.WriteLine(GetEnrichedValue(
                        ResultFormatter(task.report, resultIndex),
                        BuildTaskDictionary(task, explanation)));
                    Console.Out.Flush();

                    // Update the result code ensuring the non-zeroes will propagate to the end result.
                    if (resultIndex > this.Result)
                        this.Result = resultIndex;

                    ++finishedCount;
                    if (finishedCount == _tasks.Length) // i.e. this is the last one!
                        finishedEvent.Set();
                }
            };

            // Make the actual iteration and execution over tasks
            foreach (TaskDescription task in _tasks)
            {
                // If we're given a logger - send some info there
                if (_logger != null)
                    _logger.Write(">>> [" + task.name + "]:");

                // Make the preparation
                int resCode = ShellTask(GetEnrichedValue(task.prepare));

                if (task.exe == null)
                {
                    if (_logger != null)
                    {
                        _logger.WriteLine(" Done!");
                        _logger.WriteLine("<<< [" + task.name + "]");
                        _logger.Flush();
                    }
                    Console.Out.WriteLine(GetEnrichedValue(ResultFormatter(task.report, resCode), BuildTaskDictionary(task, DEF_PREPARE_EXPLAIN)));
                    Console.Out.Flush();
                    ++finishedCount;
                    continue;
                }

                string theExe = GetEnrichedValue(task.exe);
                string theArguments = GetEnrichedValue(task.arguments);

                if (_logger != null)
                {
                    _logger.WriteLine(" {0} {1}", theExe, theArguments);
                    _logger.Flush();
                }

                // Create and setup a task runner
                Runner runner = new Runner(theExe,  theArguments, task.wait || wait ? RunnerMode.Synchronous : RunnerMode.Asynchronous);

                runner.OnFinished = onFinish;
                runner.UserInfo = task;
                runner.SetOutputRedirect(Encoding.UTF8);
                // convert floating seconds to 100 nano-seconds ticks.
                runner.Timeout = new TimeSpan((long)((task.timeout > 0 ? task.timeout : timeout) * 1000.0 * 1000.0 * 10.0)); 

                // Setup the input, if provided.
                if (task.stdin_string != null)
                    runner.SetInput(GetEnrichedValue(task.stdin_file));
                else if (task.stdin_file != null)
                    runner.SetInput(File.ReadAllText(GetEnrichedValue(task.stdin_file)));

                // Let it fly!
                runner.Start();
            }

            finishedEvent.WaitOne();
        }

        private int Result
        {
            get;
            set;
        }

        private ParserDefinition ParseRegexDefinition(string regexString, bool multi)
        {
            char delimiter = regexString[0];
            string[] parts = regexString.Split(delimiter);
            if (parts.Length != 4)
                return null;
            var regex = new Regex(parts[1], RegexOptions.Compiled | RegexOptions.IgnoreCase);
            return new ParserDefinition(regex, parts[2], multi);
        }

        private void LoadConfiguration(string filename)
        {
            // Load and parse the configuration file.
            var serializer = new DataContractJsonSerializer(typeof(ConfigDescription));

            ConfigDescription configuration;
            using (FileStream configFile = File.Open(filename, FileMode.Open, FileAccess.Read))
                configuration = (ConfigDescription)serializer.ReadObject(configFile);

            // Iterate and convert regex definition in to Regex object
            foreach (ParserDescription entry in configuration.parsers)
            {
                var regexDef = ParseRegexDefinition(entry.regex, entry.multilined);
                if (regexDef == null)
                    throw new ArgumentException("Invalid regex provided: " + entry.regex, "regex");
                else
                    _parsers.Add(entry.name, regexDef);
            }

            // Iterate on formatters and put them in the dictionary
            foreach (FormatterDescription entry in configuration.formatters)
                _formatters.Add(entry.name, new string[] { entry.OK, entry.Failure });
        }

        private int CheckExpectations(TaskDescription task, int exitCode, string taskOutput, string taskError, out string explanation)
        {
            StringBuilder explainBuilder = new StringBuilder();
            int resultIndex = RESULT_OK;
            foreach (TaskExpectation should in task.expectations)
            {
                // Check for exit code matching
                if (should.exit_code is int && (int)should.exit_code != exitCode)
                {
                    explainBuilder.AppendFormat("[" + task.name + "]");
                    explainBuilder.AppendFormat(" exit codes doesn't match: Expecting `{0}`, but Got `{1}`!\n", should.exit_code, exitCode);
                    resultIndex = RESULT_ERR;
                }

                // Check for output stream matching
                if (should.output != null && !should.output.Equals(taskOutput))
                {
                    explainBuilder.AppendFormat("[" + task.name + "]");
                    explainBuilder.AppendFormat(" outputs doesn't match: Expecting `{0}`, but Got `{1}`!\n", should.output, taskOutput);
                    resultIndex = RESULT_ERR;
                }

                // Check for error stream matching
                if (should.error != null && !should.error.Equals(taskError))
                {
                    explainBuilder.AppendFormat("[" + task.name + "]");
                    explainBuilder.AppendFormat(" stderrs doesn't match: Expecting `{0}`, but Got `{1}`!\n", should.error, taskError);
                    resultIndex = RESULT_ERR;
                }

                // Check for provided file matching
                if (should.file != null)
                {
                    if (should.SHA1 == null && should.size == null)
                        throw new ArgumentNullException("file", "[" + task.name + "] missing both `SHA1` and `size` when a file is specified: '" + should.file + "'!");
                    try
                    {
                        if (should.SHA1 != null)
                        {
                            using (FileStream fstream = File.Open(should.file, FileMode.Open, FileAccess.Read))
                            {
                                Hasher hasher = new Hasher(fstream);
                                if (!should.SHA1.Equals(hasher.StringHash))
                                {
                                    explainBuilder.AppendFormat("[" + task.name + "]");
                                    explainBuilder.AppendFormat(" hash of file '{2}' don't match: Expecting `{0}`, but Got `{1}`!\n", should.SHA1, hasher.StringHash, should.file);
                                    resultIndex = RESULT_ERR;
                                }
                            }
                        }
                        if (should.size != null)
                        {
                            double fileLen = new FileInfo(should.file).Length;
                            Regex rex = new Regex(@"\s*([0-9.]+)\s*([PTGMK][B])?\s*", RegexOptions.Compiled | RegexOptions.IgnoreCase);
                            Match parsed = rex.Match(should.size);
                            if (parsed.Length == 0)
                                throw new ArgumentException("Unrecognized file size notation: " + should.size, "size");

                            double sz = Double.Parse(parsed.Groups[1].ToString());
                            string units = parsed.Groups[2].ToString().ToUpper();
                            if (units.Equals("PB"))
                                fileLen /= 1024.0 * 1024 * 1024 * 1024 * 1024;
                            else if (units.Equals("TB"))
                                fileLen /= 1024.0 * 1024 * 1024 * 1024;
                            else if (units.Equals("GB"))
                                fileLen /= 1024.0 * 1024 * 1024;
                            else if (units.Equals("MB"))
                                fileLen /= 1024.0 * 1024;
                            else if (units.Equals("KB"))
                                fileLen /= 1024.0;

                            if ((int)fileLen != (int)sz)
                            {
                                explainBuilder.AppendFormat("[" + task.name + "]");
                                explainBuilder.AppendFormat(" size of file '{3}' don't match: Expecting `{0}`, but Got `{1} {2}`!\n", should.size, (int)fileLen, units, should.file);
                                resultIndex = RESULT_ERR;
                            }
                        }
                    }
                    catch (IOException fileEx)
                    {
                        explainBuilder.AppendFormat("[" + task.name + "]");
                        explainBuilder.AppendFormat(" error opening file '{1}': {0}!\n", fileEx.Message, should.file);
                        resultIndex = RESULT_ERR;
                    }
                    catch (FormatException numEx)
                    {
                        throw new FormatException("Provided size cannot be understood: " + should.size, numEx);
                    }
                }
            }

            explanation = explainBuilder.ToString();
            return resultIndex;
        }

        private string GetEnrichedValue(string value, Dictionary<string, string> keys = null)
        {
            if (value == null)
                return value;
            if (keys == null)
                keys = _keys;
            foreach (KeyValuePair<string, string> pair in keys)
                value = value.Replace(pair.Key, pair.Value);
            return value;
        }

        private string ResultFormatter(string name, int index)
        {
            string[] format;
            if (name == null)
                name = FORMATDEF_NAME;
            if (!_formatters.TryGetValue(name, out format))
                return name;
            else
                return format[Math.Abs(index)];
        }

        private string ProcessStream(StreamReader stream, string[] parsersNames, string outputFile)
        {
            // First, prepare the list of parsers that we are going to use.
            List<ParserDefinition> parsers = new List<ParserDefinition>(parsersNames.Length);
            foreach (string oneName in parsersNames)
            {
                ParserDefinition oneParser;
                string finalName = GetEnrichedValue(oneName);
                if (!_parsers.TryGetValue(finalName, out oneParser))
                {
                    oneParser = ParseRegexDefinition(finalName, false);
                    if (oneParser == null)
                        throw new ArgumentException("Invalid parser referred: " + finalName, "parsers");
                }
                parsers.Add(oneParser);
            }

            // Second, setup the output writer, depending on whether output file is provided.
            TextWriter writer;
            if (outputFile != null)
                writer = File.CreateText(outputFile);
            else
                writer = new StringWriter();

            // Finally, go for the processing.
            while (!stream.EndOfStream)
            {
                string line = stream.ReadLine();

                // If we're given a logger - output the raw stream's line there.
                if (_logger != null)
                {
                    _logger.WriteLine(line);
                    _logger.Flush();
                }

                // Now go for the parsing of the line.
                foreach (var parser in parsers)
                {
                    if (parser.pattern.IsMatch(line))
                    {
                        line = parser.pattern.Replace(line, parser.subst);
                        if (parser.multilined)
                            writer.WriteLine(line);
                        else
                            writer.Write(line);
                        break;
                    }
                }
            }
            return writer.ToString();
        }

        #endregion
    }
}
